<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('id_laptop');
            $table->foreign('id_laptop')->
            references('id_laptop')->on('laptop')->
            onDelete('cascade');
            $table->string('id_transaksi')->unique();
            $table->integer('total');
            $table->integer('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_detail_transaksi');
    }
};
