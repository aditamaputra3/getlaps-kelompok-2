<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('detail_transaksi', function (Blueprint $table) {
        $table->id();
        $table->string('id_transaksi')->unique();
        $table->string('id_laptop')->unique();
        $table->integer('harga');
        $table->integer('jumlah');
        $table->timestamps();
        
        $table->foreign('id_laptop')->references('id_laptop')->on('laptop')->onDelete('cascade');
        $table->foreign('id_transaksi')->references('id_transaksi')->on('transaksi')->onDelete('cascade');
    });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
