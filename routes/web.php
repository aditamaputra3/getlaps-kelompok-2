<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\karyawanController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\LaptopController;
use App\Http\Controllers\PelangganController;
use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\DashboardHomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//index
Route::get('/', [DashboardHomeController::class, 'index']);

//Route Login
Route::get('/login', function () {
    return view('content.login');
})->name('login');

//Route Register
Route::get('/register', function () {
    return view('content.register');
});

// //Route Home
// Route::get('/dashboard-home', function () {
//     return view('content.dashboard-home');
// });

// Route::get('/dashboard-home', [DashboardHomeController::class, 'index']);


Route::post('/post-register', [RegisterController::class, 'store']);
Route::post('/post-login', [AuthController::class, 'postLogin']);
Route::get('/logout', [AuthController::class, 'logout']);

//list laptop
Route::get('/dashboard-list-laptop', [LaptopController::class, 'listLaptop']);

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard-admin', function () {
        return view('content.dashboard-admin');
    });

    Route::get('/dashboard-admin', [DashboardAdminController::class, 'getCounts'])->name('counts');

    // Route Laptop
    Route::get('/dashboard-laptop', [LaptopController::class, 'index']);
    Route::post('/dashboard-tambah-laptop', [LaptopController::class, 'store']);
    Route::get('/dashboard-edit-laptop/{id}', [LaptopController::class, 'edit']);
    Route::put('/dashboard-edit-laptop/{id}', [LaptopController::class, 'update']);
    Route::delete('/dashboard-delete-laptop/{id}', [LaptopController::class, 'delete']);

    // Route Suplier
    Route::get('/dashboard-supplier', [SupplierController::class, 'index']);
    Route::post('/dashboard-tambah-supplier', [SupplierController::class, 'store']);
    Route::get('/dashboard-edit-supplier/{id}', [SupplierController::class, 'edit']);
    Route::put('/dashboard-edit-supplier/{id}', [SupplierController::class, 'update']);
    Route::delete('/dashboard-delete-supplier/{id}', [SupplierController::class, 'delete']);

    // Route Pelanggan
    Route::get('/dashboard-pelanggan', [PelangganController::class, 'index']);
    Route::post('/dashboard-tambah-pelanggan', [PelangganController::class, 'store']);
    Route::get('/dashboard-edit-pelanggan/{id}', [PelangganController::class, 'edit']);
    Route::put('/dashboard-edit-pelanggan/{id}', [PelangganController::class, 'update']);
    Route::delete('/dashboard-delete-pelanggan/{id}', [PelangganController::class, 'delete']);

    // // Route Transaksi
    // Route::get('/dashboard-transaksi', [TransaksiController::class, 'index']);
    // Route::post('/dashboard-tambah-transaksi', [TransaksiController::class, 'store']);
    // Route::get('/dashboard-edit-transaksi/{id}', [TransaksiController::class, 'edit']);
    // Route::put('/dashboard-transaksi/{id}', [TransaksiController::class, 'update']);
    // Route::delete('/dashboard-delete-transaksi/{id}', [TransaksiController::class, 'delete']);


    Route::get('/dashboard-transaksi', [TransaksiController::class, 'index']);
    Route::post('/dashboard-tambah-transaksi', [TransaksiController::class, 'store']);
    Route::get('/dashboard-edit-transaksi/{id}', [TransaksiController::class, 'edit']);
    Route::put('/dashboard-transaksi/{id}', [TransaksiController::class, 'update']);
    Route::delete('/dashboard-delete-transaksi/{id}', [TransaksiController::class, 'delete']);



    //route karyawansss
    Route::get('/dashboard-karyawan', [karyawanController::class, 'index']);
    Route::post('/dashboard-tambah-karyawan', [karyawanController::class, 'store']);
    Route::get('/dashboard-edit-karyawan/{id}', [karyawanController::class, 'edit']);
    Route::put('/dashboard-edit-karyawan/{id}', [karyawanController::class, 'update']);
    Route::delete('/dashboard-delete-karyawan/{id}', [karyawanController::class, 'delete']);

    //transaksi
    Route::get('/dashboard-transaksi', [TransaksiController::class, 'index'])->name('transaksi.index');
    Route::get('/transaksi/create', [TransaksiController::class, 'create'])->name('transaksi.create');
    Route::post('/transaksi-add', [TransaksiController::class, 'store'])->name('transaksi.store');
    Route::delete('/transaksi/{id_transaksi}', [TransaksiController::class, 'delete'])->name('transaksi.delete');
    // Route::get('/print-transaksi', [TransaksiController::class, 'print']);
    Route::get('/print-transaksi/{id_transaksi}',  [TransaksiController::class, 'print']);



});
