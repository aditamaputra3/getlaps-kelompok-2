<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Getlaps</title>
    <link rel="icon" href="assets/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.2/dist/css/adminlte.min.css">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- IONIC Icons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    @yield('content')
</body>
<script src="https://kit.fontawesome.com/198ace4666.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.2/dist/js/adminlte.min.js"></script>
<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
@if (Session::has('success'))
    <script>
        $(function() {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: 'success',
                title: 'Data Berhasil dimasukan'
            });
        });
    </script>
@endif

@if (Session::has('successDeleted'))
    <script>
        $(function() {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: 'success',
                title: 'Data Berhasil dihapus'
            });
        });
    </script>
@endif

@if (Session::has('successUpdate'))
    <script>
        $(function() {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: 'success',
                title: 'Data Berhasil diupdate'
            });
        });
    </script>
@endif
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
        });
    });
</script>
<script>
    /*** add active class and stay opened when selected ***/
    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.nav-sidebar a').filter(function() {
        if (this.href) {
            return this.href == url || url.href.indexOf(this.href) == 0;
        }
    }).addClass('active');

    // for the treeview
    $('ul.nav-treeview a').filter(function() {
        if (this.href) {
            return this.href == url || url.href.indexOf(this.href) == 0;
        }
    }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>

@if (Request::is('dashboard-transaksi'))
    <script>
        $(document).ready(function() {
    var counter = 1;
    var laptop = @json($laptop);

    // Function untuk menambahkan baris pada form detail transaksi
    function addDetailTransaksiRow() {
        var detailTransaksi = $('#detail_transaksi');
        var row = $('<div class="row"></div>');

        var col1 = $('<div class="col"></div>');
        var select = $('<select class="form-control" name="detail_transaksi[' + counter + '][id_laptop]"></select>');
        // Isi dengan daftar laptop
        laptop.forEach(function(item) { // Iterate through laptops array
            var option = $('<option value="' + item.id_laptop + '">' + item.merek + item.tipe + '</option>');
            select.append(option);
        });
        col1.append(select);

        var col2 = $('<div class="col"></div>');
        var inputHarga = $('<input type="number" class="form-control" placeholder="Harga" name="detail_transaksi[' + counter + '][harga]" readonly>');
        col2.append(inputHarga);

        var col3 = $('<div class="col"></div>');
        var inputJumlah = $('<input type="number" class="form-control" placeholder="Jumlah" name="detail_transaksi[' + counter + '][jumlah]" required>');
        col3.append(inputJumlah);

        var col4 = $('<div class="col"></div>');
        var deleteButton = $('<button type="button" class="btn btn-danger delete-button" title="Hapus"><i class="fas fa-trash"></i></button>');
        col4.append(deleteButton);

        row.append(col1);
        row.append(col2);
        row.append(col3);
        row.append(col4);

        detailTransaksi.append(row);

        counter++;
    }

    // Menghapus baris pada form detail transaksi
    $(document).on('click', '.delete-button', function() {
        $(this).closest('.row').remove();
    });

    $('#tambah_detail_transaksi').click(function() {
        addDetailTransaksiRow();
        
    });

    // Mengubah input harga saat selection id_laptop berubah
    $('#detail_transaksi').on('change', 'select[name^="detail_transaksi["]', function() {
        var selectedLaptopId = $(this).val();
        var hargaInput = $(this).closest('.row').find('input[name^="detail_transaksi["]').eq(0);

        // Loop melalui array laptop untuk mencari harga berdasarkan ID laptop yang dipilih
        laptop.forEach(function(item) { // Iterate through laptops array
            if (item.id_laptop == selectedLaptopId) {
                hargaInput.val(item.harga);
            }
        });
    });

    $('#simpan_detail_transaksi').click(function() {
        var tabelSementara = $('#tabel_sementara');
        var rows = $('#detail_transaksi .row');
        rows.each(function(index, element) {
            var laptop = $(element).find('select').val();
            var harga = $(element).find('input[name^="detail_transaksi["]').val();
            var jumlah = $(element).find('input[name^="detail_transaksi["]').val();

            // Menambahkan data ke tabel sementara
            var row = $('<tr></tr>');
            row.append('<td>' + laptop + '</td>');
            row.append('<td>' + harga + '</td>');
            row.append('<td>' + jumlah + '</td>');
            tabelSementara.append(row);
        });

        // Mengosongkan form setelah disimpan
        $('#detail_transaksi').empty();
        counter = 1;
    });
    
});
    </script>
@endif

</html>
