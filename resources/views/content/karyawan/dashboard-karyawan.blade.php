@extends('layout.layout-admin')

@section('content')
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->

        <div class="content-wrapper" style="min-height: 2171.31px;">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Karyawan</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Karyawan</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-info">
                                <div class="card-body">
                                    <div style="min-height: 50px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal-lg">
                                            <i class="fa-solid fa-square-plus"></i> Tambah Data 
                                        </button>
                                    </div>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Id Karyawan</th>
                                                <th scope="col">Nama Karyawan </th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Jenis Kelamin</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($karyawan as $kr)
                                                <tr>
                                                    <td>{{ $kr->id_karyawan }}</td>
                                                    <td>{{ $kr->nama_karyawan }}</td>
                                                    <td>{{ $kr->alamat }}</td>
                                                    <td>{{ $kr->jenis_kelamin }}</td>
                                                    <td>
                                                        <form action="{{ url('dashboard-delete-karyawan', $kr->id) }}"
                                                            method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <a href="{{ 'dashboard-edit-karyawan/' . $kr->id }}"
                                                                class="btn btn-warning btn-sm" data-toggle="modal"
                                                                data-target="#modal-lg-edit{{ $kr->id }}">
                                                                <i class="fa-solid fa-pencil"></i> Edit</a>
                                                            <button type="submit" class="btn btn-danger btn-sm"
                                                                onclick="return confirm('Are you sure?')"><i
                                                                    class="fa fa-trash"></i> Hapus</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <!-- Modal Edit -->
                                                <div class="modal fade" id="modal-lg-edit{{ $kr->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Karyawan</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form
                                                                    action="{{ url('dashboard-edit-karyawan', $kr->id) }}"
                                                                    method="POST">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('PUT') }}
                                                                    <div class="form-group">
                                                                        <label>Id Karyawan</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Id karyawan" name="id_karyawan"
                                                                            value="{{ $kr->id_karyawan }}" readonly>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Nama Karyawan</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Nama karyawan" name="nama_karyawan"
                                                                            value="{{ $kr->nama_karyawan }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Alamat</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Alamat" name="alamat"
                                                                            value="{{ $kr->alamat }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="jenis_kelamin"
                                                                            class="form-label form-label-sm">Jenis
                                                                            Kelamin</label>
                                                                        <select class="custom-select rounded-0"
                                                                            name="jenis_kelamin" id="jenis_kelamin"
                                                                            aria-label="Default select example">
                                                                            <option value="{{ $kr->jenis_kelamin }}"
                                                                                selected disabled>{{ $kr->jenis_kelamin }}
                                                                            </option>
                                                                            <option value="Pria">Pria</option>
                                                                            <option value="Wanita">Wanita</option>
                                                                            <option value="Non-Binary">Non-Binary</option>
                                                                            <option value="Lorem-Ipsum">Lorem Ipsum</option>
                                                                        </select>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- footer -->
        @include('component.footer')
        <!-- /.footer -->
        <!-- Modal Tambah -->
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Data Karyawan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="dashboard-tambah-karyawan" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Nama Karyawan</label>
                                <input type="text" class="form-control" placeholder="Nama karyawan"
                                    name="nama_karyawan">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control" placeholder="Alamat" name="alamat">
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin" class="form-label form-label-sm">Jenis Kelamin</label>
                                <select class="custom-select rounded-0" name="jenis_kelamin" id="jenis_kelamin"
                                    aria-label="Default select example" required>
                                    <option value="Tidak Menyebutkan" selected disabled>Pilih Jenis Kelamin</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                    <option value="Non-Binary">Non-Binary</option>
                                    <option value="Lorem-Ipsum">Lorem Ipsum</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endsection
