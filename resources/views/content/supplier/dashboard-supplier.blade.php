@extends('layout.layout-admin')

@section('content')
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->

        <div class="content-wrapper" style="min-height: 2171.31px;">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Supplier</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Supplier</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-info">
                                <div class="card-body">
                                    <div style="min-height: 50px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal-lg">
                                            <i class="fa-solid fa-square-plus"></i> Tambah Data
                                        </button>
                                    </div>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID Supplier</th>
                                                <th scope="col">Nama Supplier</th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Nomor Telepon</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($supplier as $sup)
                                                <tr>
                                                    <td>{{ $sup->id_supplier }}</td>
                                                    <td>{{ $sup->nama_supplier }}</td>
                                                    <td>{{ $sup->alamat_supplier }}</td>
                                                    <td>{{ $sup->no_telp_supplier }}</td>
                                                    <td>
                                                        <form action="{{ url('dashboard-delete-supplier', $sup->id) }}"
                                                            method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <a href="{{ 'dashboard-edit-supplier/' . $sup->id }}"
                                                                data-toggle="modal"
                                                                data-target="#modal-lg-edit{{ $sup->id }}"
                                                                class="btn btn-warning btn-sm">
                                                                <i class="fa-solid fa-pencil"></i> Edit</a>
                                                            <button class="btn btn-danger btn-sm" type="submit"
                                                                onclick="return confirm('Are you sure?')">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                                Hapus</button>
                                                            {{-- <a href="{{ 'print-produk' }}"
                                                                class="btn btn-success btn-sm">Print</a> --}}
                                                        </form>
                                                    </td>
                                                </tr>
                                                <!-- Modal Edit -->
                                                <div class="modal fade" id="modal-lg-edit{{ $sup->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Supplier</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form
                                                                    action="{{ url('dashboard-edit-supplier', $sup->id) }}"
                                                                    method="POST">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('PUT') }}
                                                                    <div class="form-group">
                                                                        <label>Id Supplier</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="ID supplier" name="id_supplier"
                                                                            value="{{ $sup->id_supplier }}" readonly>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Nama Supplier</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Nama supplier" name="nama_supplier"
                                                                            value="{{ $sup->nama_supplier }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Alamat Supplier</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Alamat supplier"
                                                                            name="alamat_supplier"
                                                                            value="{{ $sup->alamat_supplier }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Nomor Telepon</label>
                                                                        <input type="number" class="form-control"
                                                                            placeholder="Nomor telepon supplier"
                                                                            name="no_telp_supplier"
                                                                            value="{{ $sup->no_telp_supplier }}">
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- footer -->
        @include('component.footer')
        <!-- /.footer -->

        <!-- Modal Tambah -->
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Data Supplier</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="dashboard-tambah-supplier" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Nama Supplier</label>
                                <input type="text" class="form-control" placeholder="Nama supplier"
                                    name="nama_supplier" required>
                            </div>
                            <div class="form-group">
                                <label>Alamat Supplier</label>
                                <input type="text" class="form-control" placeholder="Alamat supplier"
                                    name="alamat_supplier" required>
                            </div>
                            <div class="form-group">
                                <label>Nomor Telepon</label>
                                <input type="number" class="form-control" placeholder="Nomor telepon"
                                    name="no_telp_supplier" required>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endsection
