@extends('layout.layout')

@section('content')
<div class="container mt-5">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <h2 class="text-center">Register</h2>
            <form action="post-register" method="post">
                {{ csrf_field() }}
              <div class="mb-3 form-floating">
                <input type="text" class="form-control form-control-sm" name="name" id="name" placeholder="Nama" required>
                <label for="name" class="form-label">Nama</label>
              </div>
              <div class="mb-3 form-floating">
                <input type="text" class="form-control form-control-sm" name="email" id="email" placeholder="Email" required>
                <label for="name" class="form-label">Email</label>
              </div>
              <div class="mb-3 form-floating">
                <input type="password" class="form-control form-control-sm" name="password" id="password" placeholder="Password" required>
                <label for="password" class="form-label">Password</label>
              </div>
              <div class="mb-3 d-grid gap-2">
                <button type="submit" class="btn btn-dark">Register</button>
                <a href="/" class="btn btn-secondary">Kembali ke Halaman Utama</a>
              </div>
              <div class="mb-3 text-center">
                Sudah Memiliki Akun? <a href="login" class="link-primary"> Login</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

