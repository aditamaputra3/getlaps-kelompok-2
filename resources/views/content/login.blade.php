@extends('layout.layout')

@section('content')
    <div class="container mt-5">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-3 d-flex justify-content-center">
                            <h2>Login</h2>
                        </div>
                        <form action="post-login" method="post">
                            {{ csrf_field() }}
                            <div class="form-floating mb-3">
                                <input type="text" id="floatingInput" placeholder="Email" id="email" name="email"
                                    class="form-control" required />
                                <label for="floatingInput">Email</label>
                            </div>
                            <div class="form-floating mb-3">
                                <input type="password" id="password" name="password" placeholder="Password"
                                    class="form-control" required />
                                <label for="floatingInput">Password</label>
                            </div>
                            <div class="mb-3 form-check">
                                <input type="checkbox" class="form-check-input" onclick="showPass()" />
                                <label class="form-check-label" for="exampleCheck1">Show Password</label>
                            </div>
                            <div class="mb-3 d-grid gap-2">
                                <button action="submit" name="login" class="btn btn-dark">Login</button>
                                <a href="/" class="btn btn-secondary">Kembali ke Halaman Utama</a>
                            </div>
                            <div class="mb-3 d-flex justify-content-center">
                                Belum Memiliki Akun? <a href="register" class="link-primary"> Register</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container mt-4">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-fixed fade show" role="alert">
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    </div>

    </body>
@endsection
