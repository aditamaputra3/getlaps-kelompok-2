<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @foreach($transaksi as $tr)
    <title>Invoice-{{$tr->nama_pelanggan}}-{{$tr->id_transaksi}}</title>
    @endforeach
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            width: 100%;
            max-width: 800px;
            margin: 0 auto;
        }
        .header {
            text-align: center;
            margin-bottom: 20px;
        }
        .header h1 {
            margin: 0;
        }
        .invoice-info {
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
        }
        .invoice-details {
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    @php
        $trackedProducts = []; // Variabel pelacakan produk yang sudah ditampilkan
    @endphp

    @foreach($transaksi as $tr)
        @if(!in_array($tr->id_transaksi, $trackedProducts))
            <div class="container">
                <div class="header">
                    <img src="assets/getlaps.png" alt="Logo Toko" width="50px"> 
                    <h1>Getlaps</h1>
                    <h2>Invoice Pembelanjaan</h2> 
                </div>
                <hr>
                <div class="invoice-info">
                    <div>
                        <strong>ID Transaksi:</strong> {{ $tr->id_transaksi }}<br>
                        <strong>Tanggal:</strong> {{ $tr->created_at }}
                    </div>
                    <div>
                        <strong>Nama Karyawan:</strong> {{ $tr->nama_karyawan }}<br>
                        <strong>Nama Pelanggan:</strong> {{ $tr->nama_pelanggan }}
                    </div>
                </div>
                <div class="invoice-details">
                    <table>
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transaksi as $tr2)
                                @if($tr->id_transaksi == $tr2->id_transaksi)
                                    <tr>
                                        <td>{{ $tr2->merek }} {{ $tr2->tipe }}</td>
                                        <td>Rp.{{ $tr2->harga }}</td>
                                        <td>{{ $tr2->jumlah }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="invoice-info">
                    <div>
                        <h2>Total Belanja: Rp.{{ $tr->total }}</h2><br>
                    </div>
                </div>
            </div>
        @endif

        @php
            $trackedProducts[] = $tr->id_transaksi; // Tambahkan produk ke variabel pelacakan
        @endphp
    @endforeach
</body>
</html>
