@extends('layout.layout-admin')

@section('content')
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->

        <div class="content-wrapper" style="min-height: 2171.31px;">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Transaksi</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Transaksi</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-info">
                                <div class="card-body">
                                    <form id="transaksiForm" action="/transaksi-add" method="POST">
                                        @csrf
                                        <!-- Form input untuk karyawan -->
                                        <div class="form-group">
                                            <label for="id_karyawan">Karyawan:</label>
                                            <select name="id_karyawan" id="id_karyawan" class="form-control">
                                                @foreach ($karyawan as $item)
                                                    <option value="{{ $item->id_karyawan }}">{{ $item->nama_karyawan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <!-- Form input untuk pelanggan -->
                                        <div class="form-group">
                                            <label for="id_pelanggan">Pelanggan:</label>
                                            <select name="id_pelanggan" id="id_pelanggan" class="form-control">
                                                @foreach ($pelanggan as $item)
                                                    <option value="{{ $item->id_pelanggan }}">{{ $item->nama_pelanggan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <!-- Form input untuk detail transaksi -->
                                        <div class="form-group">
                                            <label for="detail_transaksi">List Produk:</label>
                                            <div id="detail_transaksi">
                                            </div>
                                        </div>

                                        <!-- Tombol tambah detail transaksi -->
                                        <div class="text-center mb-2">
                                            <button type="button" class="btn btn-primary" id="tambah_detail_transaksi">
                                                <i class="fa-solid fa-cart-plus"></i> Tambah Barang
                                            </button>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary"><i
                                                    class="fa-solid fa-square-check"></i> Selesai</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Bagian kedua card -->

            <section class="content">
                <div class="container-fluid">
                    <!-- Card untuk tabel transaksi -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-info">
                                <div class="card-header">
                                    <h3 class="card-title">Daftar Transaksi</h3>
                                </div>
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID Transaksi</th>
                                                <th>Nama Karyawan</th>
                                                <th>Nama Pelanggan</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>Total</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($transaksi->sortByDesc('created_at') as $tr)
                                                <tr>
                                                    <td>{{ $tr->id_transaksi }}</td>
                                                    <td>{{ $tr->nama_karyawan }}</td>
                                                    <td>{{ $tr->nama_pelanggan }}</td>

                                                    <td>{{ $tr->created_at }}</td>
                                                    <td>{{ $tr->total }}</td>
                                                    <td>
                                                        {{-- <form action="{{ route('transaksi.delete', ['id' => $tr->id_transaksi]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                Hapus</button> --}}
                                                        <a href="{{ 'print-transaksi/' . $tr->id_transaksi }}"
                                                            class="btn btn-success btn-sm" target="_blank"><i
                                                                class="fa-solid fa-print"></i>Cetak</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- footer -->
    @include('component.footer')
    <!-- /.footer -->

@endsection
