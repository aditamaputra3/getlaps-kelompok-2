@extends('layout.layout-admin')

@section('content')
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->

        <div class="content-wrapper" style="min-height: 2171.31px;">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Laptop</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Supplier</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-info">
                                <div class="card-body">
                                    <div style="min-height: 50px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal-lg">
                                            <i class="fa-solid fa-square-plus"></i> Tambah Data
                                        </button>
                                    </div>

                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Id Laptop</th>
                                                <th scope="col">Id Supplier</th>
                                                <th scope="col">Tipe</th>
                                                <th scope="col">Merek</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col">Ram</th>
                                                <th scope="col">Memory</th>
                                                <th scope="col">Processor</th>
                                                <th scope="col">Tanggal Rilis</th>
                                                <th scope="col">Image</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($laptop as $lap)
                                                <tr>
                                                    <td>{{ $lap->id_laptop }}</td>
                                                    <td>{{ $lap->id_supplier }}</td>
                                                    <td>{{ $lap->tipe }}</td>
                                                    <td>{{ $lap->merek }}</td>
                                                    <td>{{ $lap->harga }}</td>
                                                    <td>{{ $lap->ram }}</td>
                                                    <td>{{ $lap->memory }}</td>
                                                    <td>{{ $lap->processor }}</td>
                                                    <td>{{ $lap->tanggal_rilis }}</td>
                                                    <td>{{ $lap->image }}</td>
                                                    <td>
                                                        <form action="{{ url('dashboard-delete-laptop', $lap->id) }}"
                                                            method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <a href="{{ 'dashboard-edit-laptop/' . $lap->id }}"
                                                                class="btn btn-warning btn-sm" data-toggle="modal"
                                                                data-target="#modal-lg-edit{{ $lap->id }}">
                                                                <i class="fa-solid fa-pencil"></i> Edit</a>
                                                            <button type="submit" class="btn btn-danger btn-sm"
                                                                onclick="return confirm('Are you sure?')"><i
                                                                    class="fa fa-trash"></i> Hapus</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <!-- Modal Edit -->
                                                <div class="modal fade" id="modal-lg-edit{{ $lap->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Laptop</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ url('dashboard-edit-laptop', $lap->id) }}"
                                                                    method="POST" enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('PUT') }}
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Id Laptop</label>
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="ID Laptop" name="id_laptop"
                                                                                    value="{{ $lap->id_laptop }}" readonly>
                                                                            </div>
                                                                            <div class="form-group mb-3">
                                                                                <label>Supplier</label>
                                                                                <select class="form-control" id="id_supplier"
                                                                                name="id_supplier" required>
                                                                                @foreach($supplier as $sup)
                                                                                  <option value="{{ $lap->id_supplier }}" selected>{{ $lap->nama_supplier }}</option>
                                                                                  <option value="{{ $sup->id_supplier }}">{{ $sup->nama_supplier }}</option>
                                                                                @endforeach
                                                                              </select>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Tipe</label>
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="Tipe" name="tipe"
                                                                                    value="{{ $lap->tipe }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Merek</label>
                                                                                <select class="form-control" id="merek"
                                                                                    name="merek">
                                                                                    <option value="{{ $lap->merek }}"
                                                                                        selected>{{ $lap->merek }}
                                                                                    </option>
                                                                                    <option value="Acer">Acer</option>
                                                                                    <option value="Asus">Asus</option>
                                                                                    <option value="Dell">Dell</option>
                                                                                    <option value="HP">HP</option>
                                                                                    <option value="Lenovo">Lenovo</option>
                                                                                    <option value="Apple">Apple</option>
                                                                                    <option value="Huawei">Huawei</option>
                                                                                    <option value="Xiaomi">Xiaomi</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Harga</label>
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="Harga" name="harga"
                                                                                    value="{{ $lap->harga }}">
                                                                            </div>
                                                                    
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>RAM</label>
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="RAM" name="ram"
                                                                                    value="{{ $lap->ram }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Memory</label>
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="Memory" name="memory"
                                                                                    value="{{ $lap->memory }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Processor</label>
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="Processor"
                                                                                    name="processor"
                                                                                    value="{{ $lap->processor }}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Tanggal Rilis</label>
                                                                                <input type="date" class="form-control"
                                                                                    placeholder="Tanggal Tilis"
                                                                                    name="tanggal_rilis"
                                                                                    value="{{ $lap->tanggal_rilis }}">
                                                                            </div>
                                                                            <!-- Tampilkan gambar saat ini -->
                                                                            <div class="form-group">
                                                                                <label for="current_image">Current
                                                                                    Image:</label>
                                                                                <img src="{{ asset('images/' . $lap->image) }}"
                                                                                    alt="Current Image"
                                                                                    class="img-thumbnail">
                                                                            </div>
                                                                            <div class="form-group mb-3">
                                                                                <label>Upload New Image</label>
                                                                                <input type="file" class="form-control"
                                                                                    name="image">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close</button>
                                                                        <button type="submit"
                                                                            class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- footer -->
        @include('component.footer')
        <!-- /.footer -->

        <!-- Modal Tambah -->
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Data Laptop</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="dashboard-tambah-laptop" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label>Supplier</label>
                                        <select class="form-control" name="id_supplier" required>
                                            <option value="" selected>Pilih Supplier</option>
                                        @foreach($supplier as $sup)
                                          <option value="{{ $sup->id_supplier }}">{{ $sup->nama_supplier }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Tipe</label>
                                        <input type="text" class="form-control" name="tipe" placeholder="Tipe">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Merek</label>
                                        <select class="form-control" id="merek" name="merek" required>
                                            <option value="">Pilih Merek</option>
                                            <option value="Acer">Acer</option>
                                            <option value="Asus">Asus</option>
                                            <option value="Dell">Dell</option>
                                            <option value="HP">HP</option>
                                            <option value="Lenovo">Lenovo</option>
                                            <option value="Apple">Apple</option>
                                            <option value="Huawei">Huawei</option>
                                            <option value="Xiaomi">Xiaomi</option>
                                        </select>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Harga</label>
                                        <input type="text" class="form-control" name="harga" placeholder="Harga">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Layar</label>
                                        <input type="number" class="form-control" name="layar" placeholder="Layar">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label>RAM</label>
                                        <input type="text" class="form-control" name="ram" placeholder="RAM">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Memory</label>
                                        <input type="text" class="form-control" name="memory" placeholder="Memory">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Processor</label>
                                        <input type="text" class="form-control" name="processor" placeholder="Processor">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Tanggal Rilis</label>
                                        <input type="date" class="form-control" name="tanggal_rilis"
                                            placeholder="Tanggal Rilis">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label>Upload Image</label>
                                        <input type="file" class="form-control" name="image">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endsection
