@extends('layout.layout')

@section('content')
    @include('component.navbar')

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1>List Laptop</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($laptop as $lap)
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <img src="{{ asset('images/' . $lap->image) }}" alt="Current Image" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">{{ $lap->merek }} {{ $lap->tipe }}</h5>
                            <p class="card-text">RAM: {{ $lap->ram }} - Processor : {{ $lap->processor }}</p>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#ModalLaptop{{ $lap->id_laptop }}">Lihat Selengkapnya</button>
                        </div>
                    </div>
                </div>

                {{-- Modal Laptop --}}
                <div class="modal fade" id="ModalLaptop{{ $lap->id_laptop }}" tabindex="-1"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $lap->merek }} {{ $lap->tipe }}</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <img src="{{ asset('images/' . $lap->image) }}" alt="Current Image"
                                                class="img-thumbnail">
                                        </div>
                                        <div class="col-md-6">
                                            <h6>Spesifikasi</h6>
                                            <ul>
                                                <li>Merek: {{ $lap->merek }}</li>
                                                <li>Layar: {{ $lap->layar }}</li>
                                                <li>RAM: {{ $lap->ram }}</li>
                                                <li>Memory: {{ $lap->memory }}</li>
                                                <li>Processor: {{ $lap->processor }}</li>
                                                <li>Tanggal Rilis: {{ $lap->tanggal_rilis }}</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <h4>Harga : Rp.{{ $lap->harga }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Modal Laptop  --}}
            @endforeach
        </div>


        <div class="container">
            <footer class="row row-cols-1 row-cols-sm-2 row-cols-md-5 py-5 my-5 border-top">
                <div class="col mb-3">
                    <a href="/" class="d-flex align-items-center mb-3 link-dark text-decoration-none">
                        <img width="40" src="assets/getlaps.png" />
                        <h3>Getlaps</h3>
                    </a>
                    <p class="text-muted">© 2023</p>
                </div>

                <div class="col mb-3">

                </div>

                <div class="col mb-3">
                    <h5>Section</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Rumah</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Pricing</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">FAQs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">About</a></li>
                    </ul>
                </div>

                <div class="col mb-3">
                    <h5>Section</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Pricing</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">FAQs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">About</a></li>
                    </ul>
                </div>

                <div class="col mb-3">
                    <h5>Section</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Pricing</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">FAQs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">About</a></li>
                    </ul>
                </div>
            </footer>
        </div>
    @endsection
