@extends('layout.layout-admin')

@section('content')
    <div class="wrapper">
        <!-- navbar -->
        @include('component.navbar-admin')
        <!-- /.navbar -->

        <!-- sidebar -->
        @include('component.sidebar')
        <!-- /.sidebar -->
        <div class="content-wrapper" style="min-height: 2171.31px;">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Pelanggan</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Pelanggan</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-outline card-info">
                                <div class="card-body">
                                    <div style="min-height: 50px;">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#modal-lg">
                                            <i class="fa-solid fa-square-plus"></i> Tambah Data
                                        </button>
                                    </div>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>

                                                <th scope="col">Id</th>
                                                <th scope="col">Nama Pelanggan</th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Jenis Kelamin</th>
                                                <th scope="col">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($pelanggan as $plg)
                                                <tr>
                                                    <td>{{ $plg->id_pelanggan }} </td>
                                                    <td>{{ $plg->nama_pelanggan }} </td>
                                                    <td>{{ $plg->alamat }} </td>
                                                    <td>{{ $plg->jenis_kelamin }} </td>
                                                    <td>
                                                        <form action="{{ url('dashboard-delete-pelanggan', $plg->id) }}"
                                                            method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('delete') }}
                                                            <a href="#" class="btn btn-warning btn-sm"
                                                                data-toggle="modal"
                                                                data-target="#modal-lg-edit{{ $plg->id }}">
                                                                <i class="fa-solid fa-pencil"></i> Edit</a>
                                                            <button class="btn btn-danger btn-sm" type="submit"
                                                                onclick="return confirm('Are you sure?')">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                                Hapus</button>
                                                            {{-- <a href="{{ 'print-produk' }}"
                                                            class="btn btn-success btn-sm">Print</a> --}}
                                                        </form>
                                                    </td>
                                                </tr>
                                                <!-- Modal Edit -->
                                                <div class="modal fade" id="modal-lg-edit{{ $plg->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Edit Data Pelanggan</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form
                                                                    action="{{ url('dashboard-edit-pelanggan', $plg->id) }}"
                                                                    method="POST">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('PUT') }}
                                                                    <div class="form-group">
                                                                        <label>Id Pelanggan</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="ID" name="id_pelanggan"
                                                                            value="{{ $plg->id_pelanggan }}" readonly>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Nama Pelanggan</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Nama Pelanggan"
                                                                            name="nama_pelanggan"
                                                                            value="{{ $plg->nama_pelanggan }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Alamat</label>
                                                                        <input type="text" class="form-control"
                                                                            placeholder="Alamat" name="alamat"
                                                                            value="{{ $plg->alamat }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="jenis_kelamin"
                                                                            class="form-label form-label-sm">Jenis
                                                                            Kelamin</label>
                                                                        <select class="custom-select rounded-0"
                                                                            name="jenis_kelamin" id="jenis_kelamin"
                                                                            aria-label="Default select example" required>
                                                                            <option value="{{ $plg->jenis_kelamin }}"
                                                                                selected disabled>{{ $plg->jenis_kelamin }}
                                                                            </option>
                                                                            <option value="Pria">Pria</option>
                                                                            <option value="Wanita">Wanita</option>
                                                                            <option value="Non-Binary">Non-Binary</option>
                                                                            <option value="Lorem-Ipsum">Lorem Ipsum</option>
                                                                        </select>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- footer -->
        @include('component.footer')
        <!-- /.footer -->

        <!-- Modal Tambah -->
        <div class="modal fade" id="modal-lg">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Data Pelanggan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="dashboard-tambah-pelanggan" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Nama pelanggan</label>
                                <input type="text" class="form-control" placeholder="Nama pelanggan"
                                    name="nama_pelanggan" required>
                            </div>
                            <div class="form-group">
                                <label>Alamat pelanggan</label>
                                <input type="text" class="form-control" placeholder="Alamat pelanggan" name="alamat"
                                    required>
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin" class="form-label form-label-sm">Jenis Kelamin</label>
                                <select class="custom-select rounded-0" name="jenis_kelamin" id="jenis_kelamin"
                                    aria-label="Default select example" required>
                                    <option value="Tidak Menyebutkan" selected disabled>Pilih Jenis Kelamin</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                    <option value="Non-Binary">Non-Binary</option>
                                    <option value="Lorem-Ipsum">Lorem Ipsum</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endsection
