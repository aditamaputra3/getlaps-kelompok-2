<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\laptop;
use App\Models\Supplier;

class LaptopController extends Controller
{
    public function index()
    {
        $data2['supplier'] = supplier::all();
        $data['laptop'] = Laptop::join('supplier', 'laptop.id_supplier', '=', 'supplier.id_supplier')
        ->select('laptop.*', 'supplier.nama_supplier')
        ->get();
        return view('content.laptop.dashboard-laptop', $data,$data2);
    }

    public function listLaptop()
    {
        $data['laptop'] = laptop::all();
        return view('content.laptop.dashboard-list-laptop', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Validasi file gambar
        ]);

        $input = $request->all();

        // Cek apakah ada file yang diupload
        if ($request->hasFile('image')) {
            $image = $request->file('image');

            // Generate nama unik untuk file gambar
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            // Simpan gambar ke folder "public" menggunakan method "move"
            $image->move(public_path('images'), $imageName);

            // Simpan nama file gambar ke dalam input array
            $input['image'] = $imageName;
        } else {
            //simpan dengan nama default.png jika tidak ada gambar yang disimpan
            $input['image'] = 'default.png';
        }

        try {
            $input['id_laptop'] = $this->generateLaptopId(); // Generate the automatic laptop ID
            laptop::create($input)->save();
            session()->flash('success', 'Data added successfully.');
            return redirect('dashboard-laptop')->with('success', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to add data.');
            return redirect()->back();
        }
    }

    protected function generateLaptopId()
    {
        $lastLaptop = Laptop::latest('id_laptop')->first();

        if (!$lastLaptop) {
            return 'LP-001'; // If no previous laptop exists, start with LP-001
        }

        $lastId = intval(substr($lastLaptop->id_laptop, 3)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'LP-' . $paddedNewId;

        return $generatedId;
    }

    public function edit($id)
    {
        $data['laptop'] = laptop::find($id);
        return view('dashboard-laptop', $data);
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->all();
            $laptop = laptop::find($id);

            if ($request->hasFile('image')) {
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Validasi file gambar
                ]);

                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images'), $imageName); // Simpan gambar ke folder 'public/images'

                // Hapus gambar lama jika ada, kecuali jika nama filenya adalah 'default.png'
            if (!empty($laptop->image) && $laptop->image !== 'default.png') {
                $oldImagePath = public_path('images') . '/' . $laptop->image;
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }

                $input['image'] = $imageName; // Menyimpan nama file gambar baru ke dalam input
            }

            $laptop->update($input);
            session()->flash('successUpdate', 'Data updated successfully.');
            return redirect('dashboard-laptop')->with('successUpdate', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to update data.');
            return redirect('dashboard-laptop')->with('error', true);
        }
    }


    public function delete($id, Request $request)
    {
        $user = laptop::find($id);
        try {
            $user->delete($request->all());
            session()->flash('successDeleted', 'Data deleted successfully.');
            return redirect('dashboard-laptop')->with('successDeleted', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to delete data.');
            return redirect()->back();
        }
    }
}
