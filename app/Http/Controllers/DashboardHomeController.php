<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\laptop;

class DashboardHomeController extends Controller
{
    public function index()
    {
        $data['laptop'] = laptop::orderBy('tanggal_rilis', 'desc')->take(3)->get();
        return view('content.dashboard-home', $data);
    }
}
