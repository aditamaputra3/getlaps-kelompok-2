<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function index()
    {
        $data['pelanggan'] = Pelanggan::all();
        return view('content.pelanggan.dashboard-pelanggan', $data);
    }

    public function create()
    {
        return view('content.pelanggan.dashboard-pelanggan');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $input['id_pelanggan'] = $this->generatePelangganId();
            Pelanggan::create($input);
            session()->flash('success', 'Data added successfully.');
            return redirect('dashboard-pelanggan')->with('success', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to add data.');
            return redirect()->back();
        }
    }

    protected function generatePelangganId()
    {
        $lastPelanggan = Pelanggan::latest('id_pelanggan')->first();

        if (!$lastPelanggan) {
            return 'PLG-001'; // If no previous pelanggan exists, start with PLG-001
        }

        $lastId = intval(substr($lastPelanggan->id_pelanggan, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'PLG-' . $paddedNewId;

        return $generatedId;
    }


    public function edit($id)
    {
        $data['pelanggan'] = pelanggan::find($id);
        return view('dashboard-pelanggan', $data);
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->all();
            Pelanggan::find($id)->update($input);
            session()->flash('successUpdate', 'Data updated successfully.');
            return redirect('dashboard-pelanggan')->with('successUpdate', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to update data.');
            return redirect()->back();
        }
    }

    public function delete($id, Request $request)
    {
        $user = Pelanggan::find($id);
        try {
            $user->delete($request->all());
            session()->flash('successDeleted', 'Data deleted successfully.');
            return redirect('dashboard-pelanggan')->with('successDeleted', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to delete data.');
            return redirect()->back();
        }
    }

    // public function getCountPlg()
    // {
    //     $countPlg = Pelanggan::count();
    //     return view('content.dashboard-admin', compact('countPlg'));
    // }
}
