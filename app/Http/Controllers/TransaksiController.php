<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\DetailTransaksi;
use App\Models\karyawan;
use App\Models\laptop;
use App\Models\Pelanggan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade\Pdf;

class TransaksiController extends Controller
{
    public function index()
{
    // Mendapatkan daftar karyawan dan pelanggan dari database
    $karyawan = Karyawan::all();
    $pelanggan = Pelanggan::all();
    $laptop = Laptop::all();
    $transaksiAll = Transaksi::all();

    $transaksiQuery = Transaksi::join('karyawan', 'transaksi.id_karyawan', '=', 'karyawan.id_karyawan')
        ->join('pelanggan', 'transaksi.id_pelanggan', '=', 'pelanggan.id_pelanggan')
        ->join('detail_transaksi', 'transaksi.id_transaksi', '=', 'detail_transaksi.id_transaksi')
        ->join('laptop', 'detail_transaksi.id_laptop', '=', 'laptop.id_laptop')
        ->select(
            'transaksi.id_transaksi',
            'karyawan.nama_karyawan',
            'pelanggan.nama_pelanggan',
            'transaksi.total',
            'laptop.merek',
            'laptop.tipe',
            'transaksi.created_at'
        );

    $transaksi = $transaksiQuery->get()->groupBy('id_transaksi')->map(function ($group) {
        return $group->first();
    });

    return view('content.transaksi.dashboard-transaksi', compact('karyawan', 'pelanggan', 'transaksi', 'laptop', 'transaksiAll'));
}


    public function store(Request $request)
    {
        // Validasi data yang dikirim oleh user
        $request->validate([
            'id_karyawan' => 'required',
            'id_pelanggan' => 'required',
            'detail_transaksi' => 'required|array',
            'detail_transaksi.*.id_laptop' => 'required',
            'detail_transaksi.*.harga' => 'required',
            'detail_transaksi.*.jumlah' => 'required',
        ]);

        // Membuat transaksi dengan id yang di-generate
        $transaksi = Transaksi::create([
            'id_transaksi' => Str::uuid(),
            'id_karyawan' => $request->id_karyawan,
            'id_pelanggan' => $request->id_pelanggan,
            'total' => 0, // Akan diupdate setelah menambahkan detail transaksi
        ]);

        // Menyimpan detail transaksi
        $detailTransaksi = $request->detail_transaksi;
        if (!is_null($detailTransaksi)) {
            $total = 0;
            foreach ($detailTransaksi as $detail) {
                DetailTransaksi::create([
                    'id_transaksi' => $transaksi->id_transaksi,
                    'id_laptop' => $detail['id_laptop'],
                    'harga' => $detail['harga'],
                    'jumlah' => $detail['jumlah'],
                ]);

                $subtotal = $detail['harga'] * $detail['jumlah'];
                $total += $subtotal;
            }

            // Mengupdate total pada transaksi
            $transaksi->total = $total;
            $transaksi->save();
        }

        return redirect()->route('transaksi.index')->with('success', 'Transaksi berhasil disimpan.');
    }

    public function print($id_transaksi)
    {
        // Pembuatan data invoice
        $transaksi = Transaksi::join('karyawan', 'transaksi.id_karyawan', '=', 'karyawan.id_karyawan')
            ->join('pelanggan', 'transaksi.id_pelanggan', '=', 'pelanggan.id_pelanggan')
            ->join('detail_transaksi', 'transaksi.id_transaksi', '=', 'detail_transaksi.id_transaksi')
            ->join('laptop', 'detail_transaksi.id_laptop', '=', 'laptop.id_laptop')
            ->select(
                'transaksi.id_transaksi',
                'karyawan.nama_karyawan',
                'pelanggan.nama_pelanggan',
                'transaksi.total',
                'laptop.merek',
                'laptop.tipe',
                'transaksi.created_at',
                'detail_transaksi.jumlah',
                'detail_transaksi.harga'
            )
            ->where('transaksi.id_transaksi', $id_transaksi)
            ->get();

        // Generate invoice PDF
        $pdf = PDF::loadView('content.transaksi.invoice', ['transaksi' => $transaksi]);
        $pdf->setPaper('A4', 'portrait');

        // Kembalikan response sebagai download link atau pesan sukses
        return $pdf->stream();
    }



    public function delete($id_transaksi)
    {
        $transaksi = Transaksi::find($id_transaksi);

        if ($transaksi) {
            // Menghapus transaksi dan detailnya
            $transaksi->delete();

            return redirect()->route('transaksi.index')->with('success', 'Transaksi berhasil dihapus.');
        } else {
            return redirect()->route('transaksi.index')->with('error', 'Transaksi tidak ditemukan.');
        }
    }
}
