<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;

class SupplierController extends Controller
{
    //
    public function index()
    {
        $data['supplier'] = Supplier::all();
        return view('content.supplier.dashboard-supplier', $data);
    }

    public function create()
    {
        return view('content.supplier.dashboard-tambah-supplier');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $input['id_supplier'] = $this->generateSupplierId(); // Generate the automatic supplier ID
            Supplier::create($input);
            session()->flash('success', 'Data added successfully.');
            return redirect('dashboard-supplier')->with('success', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to add data.');
            return redirect()->back();
        }
    }

    protected function generateSupplierId()
    {
        $lastSupplier = Supplier::latest('id_supplier')->first();

        if (!$lastSupplier) {
            return 'SUP-001'; // If no previous supplier exists, start with SUP-001
        }

        $lastId = intval(substr($lastSupplier->id_supplier, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'SUP-' . $paddedNewId;

        return $generatedId;
    }

    public function edit($id)
    {
        $data['supplier'] = Supplier::find($id);
        return view('content.supplier.dashboard-edit-supplier', $data);
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->all();
            Supplier::find($id)->update($input);
            session()->flash('successUpdate', 'Data updated successfully.');
            return redirect('dashboard-supplier')->with('successUpdate', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to update data.');
            return redirect()->back();
        }
    }

    public function delete($id, Request $request)
    {
        $user = Supplier::find($id);
        try {
            $user->delete($request->all());
            session()->flash('successDeleted', 'Data deleted successfully.');
            return redirect('dashboard-supplier')->with('successDeleted', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to delete data.');
            return redirect()->back();
        }
    }

    // public function getCountSup()
    // {
    //     $countSup = Supplier::count();
    //     return view('content.dashboard-admin', compact('countSup'));
    // }
}
