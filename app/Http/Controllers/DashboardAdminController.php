<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Pelanggan;
use App\Models\Transaksi;
use App\Models\karyawan;


class DashboardAdminController extends Controller
{
    public function getCounts()
{
    $supplierCount = Supplier::count();
    $pelangganCount = Pelanggan::count();
    $transaksiCount = Transaksi::count();
    $pegawaiCount = karyawan::count();

    return view('content.dashboard-admin', compact('transaksiCount', 'supplierCount', 'pelangganCount', 'pegawaiCount'));
}
}
