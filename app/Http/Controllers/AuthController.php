<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function postLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session('')->regenerate();

            return redirect()->intended('dashboard-admin');
        }

        return back()->withErrors([
            'username' => 'The provided crendetials do not match our record.',
        ])->onlyInput('username');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
