<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\karyawan;

class karyawanController extends Controller
{

    public function index()
    {
        $data['karyawan'] = karyawan::all();
        return view('content.karyawan.dashboard-karyawan', $data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $input['id_karyawan'] = $this->generateKaryawanId(); // Generate the automatic karyawan ID
            karyawan::create($input);
            session()->flash('success', 'Data added successfully.');
            return redirect('dashboard-karyawan')->with('success', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to add data.');
            return redirect()->back();
        }
    }

    protected function generateKaryawanId()
    {
        $lastKaryawan = Karyawan::latest('id_karyawan')->first();

        if (!$lastKaryawan) {
            return 'KRW-001'; // If no previous karyawan exists, start with KRW-001
        }

        $lastId = intval(substr($lastKaryawan->id_karyawan, 4)); // Extract the numeric portion of the last ID
        $newId = $lastId + 1;
        $paddedNewId = str_pad($newId, 3, '0', STR_PAD_LEFT); // Pad the new ID with leading zeros if necessary
        $generatedId = 'KRW-' . $paddedNewId;

        return $generatedId;
    }


    public function edit($id)
    {
        $data['karyawan'] = karyawan::find($id);
        return view('content.karyawan.dashboard-edit-karyawan', $data);
    }

    public function update($id, Request $request)
    {
        try {
            $input = $request->all();
            karyawan::find($id)->update($input);
            session()->flash('successUpdate', 'Data updated successfully.');
            return redirect('dashboard-karyawan')->with('successUpdate', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to update data.');
            return redirect()->back();
        }
    }

    public function delete($id, Request $request)
    {
        $user = karyawan::find($id);
        try {
            $user->delete($request->all());
            session()->flash('successDeleted', 'Data deleted successfully.');
            return redirect('dashboard-karyawan')->with('successDeleted', true);
        } catch (\Exception $e) {
            session()->flash('error', 'Failed to delete data.');
            return redirect()->back();
        }
    }
}
